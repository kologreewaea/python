# Допуск к промежуточной аттестации по 1 модулю «Основы программирования и алгоритмов»
import pandas as pd


class Main:
    file_name = ''
    file = pd.DataFrame()

    def __init__(self):
        action = 0
        while action != 5:
            action = self.get_action()
            print(self.do_action(action))

    def do_action(self, action: int):
        if action == 1:
            print('Укажите имя файла: ', end='')
            self.file_name = input()
            return f"{self.init_file()} товаров загружено."

        elif action == 2:
            print('Дешевые товары:')
            return self.file.sort_values(by=['Price']).head(5)
        elif action == 3:
            print('Введите часть названия товара:')
            return self.filter(input())
        elif action == 4:
            pass

        return 'До свидания!'

    def get_action(self):
        print('Выберите действие: ', end='')
        return int(input())

    def init_file(self):
        self.file = pd.read_csv(self.file_name, index_col=0)
        return len(self.file.index)

    def filter(self, text):
        result = self.file.loc[self.file.Name.map(lambda p: p.find(text) > -1)]

        print('Найдено товаров ', len(result))

        if len(result) > 0:
            return result.head(5)



