# # Задача №354. Побочная диагональ
# # Дано число n, n \(\leq\) 100 . Создайте массив n×n и заполните его по следующему правилу:
# # - числа на диагонали, идущей из правого верхнего в левый нижний угол, равны 1;
# # - числа, стоящие выше этой диагонали, равны 0;
# # - числа, стоящие ниже этой диагонали, равны 2.
# #
# # Входные данные
# # Программа получает на вход число n.
# #
# # Выходные данные
# # Необходимо вывести  полученный массив. Числа разделяйте одним пробелом.
# #
# входные данные
# 4
# выходные данные
# 0 0 0 1
# 0 0 1 2
# 0 1 2 2
# 1 2 2 2

def matrix(i, j):
    if j + i == n - 1:
        return 1
    elif j + i < n - 1:
        return 0
    else:
        return 2


n = int(input())

tab = [[matrix(i, j) for i in range(n)] for j in range(n)]

for row in tab:
    for x in row:
        print(x, end="\t")
    print()
