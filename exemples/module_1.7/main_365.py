# Задача №365. Заполнение спиралью
# Дано число n. Создайте массив A[2*n+1][2*n+1] и заполните его по спирали, начиная с числа 0 в центральной клетке
# A[n+1][n+1]. Спираль выходит вверх, далее закручивается против часовой стрелки.
# Входные данные
# Программа получает на вход одно число n.
#
# Выходные данные
# Программа должна вывести  полученный массив, отводя на вывод каждого числа ровно 3 символа.
#
#
# n = int(input())
# row, col = 2 * n, 2 * n
# A = [[0] * row] * col
# i, j = n + 1, n + 1
# x = 0
def spiral_mtrx(size):
    mtrx = [[0]]
    num = 2
    for i in range(1, size):
        if i & 1:
            for row in mtrx:
                row.append(num)
                num += 1

            mtrx.append(list(range(num + i, num - 1, -1)))
            num += i + 1
        else:
            for row in reversed(mtrx):
                row.insert(0, num)
                num += 1

            mtrx.insert(0, list(range(num, num + i + 1)))
            num += i + 1
    return mtrx

n = int(input())
print(*spiral_mtrx(2 * n + 1), sep='\n')