# Задача №2937. Следующее и предыдущее
# Напишите программу, которая считывает целое число и выводит текст, аналогичный приведенному в примере.
# Пробелы, знаки препинания, заглавные и строчные буквы важны!

def prev(x):
    return int(x) - 1


def next(x):
    return int(x) + 1


a = input()

print(f"The next number for the number {a} is {next(a)}.")
print("The previous number for the number ", a, " is ", prev(a), ".")
