# Задача №112149. Расстояние
# Напишите программу, которая вводит координаты двух точек на числовой оси и выводит расстояние между ними.

# AB = √(xb - xa)2 + (yb - ya)2
import math


def distance(x1, y1, x2, y2):
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)


xT1, yT1 = map(float, input().split())  # ввод координат точки Т1
xT2, yT2 = map(float, input().split())  # ввод координат точки Т2

print(f"{distance(xT1, yT1, xT2, yT2):.3f}")