# Задача №112146. Круг
# Напишите программу, которая вводит радиус круга и вычисляет его площадь и длину окружности.

import math


def radius(r):
    return round(math.pi * (r ** 2), 3)


def square(r):
    return round(2 * math.pi * r, 3)


a = float(input())

print(radius(a))
print(square(a))
