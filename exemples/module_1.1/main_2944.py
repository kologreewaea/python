# Задача №2944. Сумма цифр
# Дано трехзначное число. Найдите сумму его цифр.

def sum(x):
    result = 0
    while ((x // 10) > 0):
        result += x % 10
        x = x // 10
    return result + x


a = int(input())
print(sum(a))
