# Задача №2936. Гипотенуза
# Дано два числа a и b. Найдите гипотенузу треугольника с заданными катетами.

def hypotenuse(a, b):
    import math
    return math.sqrt(a ** 2 + b ** 2)


a = int(input())
b = int(input())

print(hypotenuse(a, b))
