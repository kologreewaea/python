# Задача №132. Сумма чисел
# Напишите программу, которая вычисляет сумму двух целых неотрицательных чисел
# (длина каждого из чисел не превосходит 250)
# Модуль 1. Базовые конструкции языка программирования
# 1.1 Ввод-вывод. Переменные, выражения, операции с числами
def add(x, y):
    return int(x) + int(y)


a = input()
b = input()

result = add(a, b)
print(result)
