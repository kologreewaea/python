# Задача №3740. Второе вхождение
# Дана строка. Найдите в этой строке второе вхождение буквы f, и выведите индекс этого вхождения. Если буква f в
# данной строке встречается только один раз, выведите число -1, а если не встречается ни разу, выведите число -2.
#
#
# Входные данные
# Вводится строка.
#
# Выходные данные
# Выведите ответ на задачу.
#

needle = 'f'
haystack = input()

first = haystack.find(needle)
if first < 0:
    two = -2
else:
    two = haystack.find(needle, first + 1)

print(two)
