# Задача №3024. Периметр треугольника
# Напишите функцию, вычисляющую длину отрезка по координатам его концов. С помощью этой функции напишите программу,
# вычисляющую периметр треугольника по координатам трех его вершин.
#
# Входные данные
# На вход программе подается 6 целых чисел — координат x1,y1,x2,y2,x3,y3 вершин треугольника. Все числа по модулю
# не превосходят 30000.
#
# Выходные данные
# # Выведите значение периметра этого треугольника с точностью до 6 знаков после десятичной точки.

import math


def distance(x1, y1, x2, y2) -> float:
    '''
    рассчитывает расстояние между 2 точками
    :param x1: float
    :param y1: float
    :param x2: float
    :param y2: float
    :return: float
    '''
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)


x1, y1, x2, y2, x3, y3 = map(float, input().split())
a = distance(x1, y1, x2, y2)
b = distance(x2, y2, x3, y3)
c = distance(x3, y3, x1, y1)

print(f"{a + b + c:.6f}")