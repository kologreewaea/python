# Задача №307. Степень
# Напишите функцию double power (double a, int n) (C/C++), function power (a:real; n:longint): real (Pascal),
# вычисляющую значение an.
# Входные данные
# Вводится 2 числа - a (вещественное) и n (целое неотрицательное).
#
# Выходные данные
# Необходимо вывести  значение an.

def pwr(a:float,n:int):
    """
    :type a: float
    :type n: int
    :return float
    """

    return a**n

a, n = input().split()
print(pwr(float(a), int(n)))