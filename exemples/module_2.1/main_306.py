# Задача №306. Минимум 4 чисел
# Напишите функцию int min (int a, int b, int c, int d) (C/C++), static int min (int a, int b, int c,
# int d) (Java) function min (a,b,c,d: integer):integer (Pascal), находящую наименьшее из четырех данных чисел.
#
# Входные данные
# Вводится четыре числа.
#
# Выходные данные
# Необходимо вывести  наименьшее из 4-х данных чисел.

def main_min(x):
    return min([int(i) for i in x.split()])


x = input()
print(main_min(x))
