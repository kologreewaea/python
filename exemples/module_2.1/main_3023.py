# Задача №3023. НОД n чисел с подпрограммой
# Напишите функцию для нахождения наибольшего общего делителя двух чисел с помощью алгоритма Евклида и используйте
# ее в программе для нахождения НОД уже n чисел.
#
# Входные данные
# На вход программе сначала подается значение n (2≤n≤100). В следующей строке находятся n целых неотрицательных чисел,
# не превосходящих 30000.
#
# Выходные данные
# Выведите НОД исходных чисел.

def nod(a, b) -> int:
    while a != 0 and b != 0:
        if a > b:
            a = a % b
        else:
            b = b % a

    return a + b


def nod_n(items: list) -> int:
    res = set()
    for i in range(1, len(items)):
        res.add(nod(items[i - 1], items[i]))
    return min(res)


n = int(input())
items = [int(x) for x in input().split()]

print(nod_n(items))
