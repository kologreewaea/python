# Задача №3764. Частотный анализ
# Дан текст. Выведите все слова, встречающиеся в тексте, по одному на каждую строку. Слова должны быть отсортированы
# по убыванию их количества появления в тексте, а при одинаковой частоте появления — в лексикографическом порядке.
#
# Указание. После того, как вы создадите словарь всех слов, вам захочется отсортировать его по частоте встречаемости
# слова. Желаемого можно добиться, если создать список, элементами которого будут кортежи из двух элементов:
# частота встречаемости слова и само слово. Например, [(2, 'hi'), (1, 'what'), (3, 'is')].
# Тогда стандартная сортировка будет сортировать список кортежей, при этом кортежи сравниваются по первому элементу,
# а если они равны — то по второму. Это почти то, что требуется в задаче.
#
# Входные данные
# Вводится текст.
#
# Выходные данные
# Выведите ответ на задачу.
import sys

words = {}

for s in sys.stdin:
    for w in s.split():
        words[w] = words.get(w, 0) + 1


items = [(-v, k) for k, v in words.items()]
print('\n'.join([item[1] for item in sorted(items)]))
