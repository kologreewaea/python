# Задача №302. Тип треугольника
# Определите тип треугольника (остроугольный, тупоугольный, прямоугольный) с данными сторонами.
#
# Входные данные
# Даны три натуральных числа – стороны треугольника.
#
# Выходные данные
# Необходимо вывести одно из слов: right для прямоугольного треугольника, acute для остроугольного треугольника,
# obtuse для тупоугольного треугольника или impossible, если входные числа не образуют треугольника.
from array import array


def is_triangle(a: int, b: int, c: int) -> bool:
    return (a + b > c) & (b + c > a) & (a + c > b)


def grade(a: int, b: int, c: int) -> str:
    """

    :type a: int
    :type b: int
    :type c: int
    :return str
    """
    import math
    n = (a ** 2 + b ** 2 - c ** 2) / (2 * a * b)

    return math.degrees(math.acos(n))


def type_triangle(a, b, c):
    RIGHT = 'right'
    ACUTE = 'acute'
    OBTUS = 'obtuse'
    IMPOS = 'impossible'

    if not (is_triangle(a, b, c)):
        return IMPOS

    Triangle = max(grade(a, b, c), grade(c, a, b), grade(b, c, a))

    if Triangle == 90:
        return RIGHT
    elif Triangle > 90:
        return OBTUS
    elif Triangle < 90:
        return ACUTE


a = float(input())
b = float(input())
c = float(input())

print(type_triangle(b, c, a))
