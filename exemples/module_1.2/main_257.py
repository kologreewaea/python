# Задача №257. Конь
# Требуется определить, бьет ли конь, стоящий на клетке с указанными координатами (номер строки и номер столбца),
# фигуру, стоящую на другой указанной клетке.
#
# Входные данные
# Вводятся четыре числа: координаты коня и координаты другой фигуры. Все координаты - целые числа в интервале от 1 до 8.
#
# Выходные данные
# Программа должна вывести слово YES, если конь может побить фигуру за 1 ход, в противном случае вывести слово NO.

def horse(x1: int, y1: int, x2: int, y2: int):
    """
    :return :str
    :type x1: int
    :type y1: int
    :type x2: int
    :type y2: int
    """

    YES: str = 'YES'
    NO: str = 'NO'

    if {abs(x1 - x2), abs(y1 - y2)} == {1, 2}:
        return YES
    else:
        return NO


x1 = int(input())
y1 = int(input())

x2 = int(input())
y2 = int(input())

print(horse(x1, y1, x2, y2))

# import itertools
#
# stuff = [1, 2, 3, 4, 5, 6, 7, 8]
# for L in range(0, len(stuff) + 1):
#     for subset in itertools.combinations(stuff, L):
#         if len(subset) == 2:
#             print(subset[0], subset[1], horse(4, 5, subset[0], subset[1], ))
