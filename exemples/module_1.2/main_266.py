# Задача №266. Координатные четверти
# Даны координаты двух точек на плоскости, требуется определить, лежат ли они в одной координатной четверти или нет
# (все координаты отличны от нуля).
#
# Входные данные
# Вводятся 4 числа: координаты первой точки (x1, y1) и координаты второй точки (x2, y2).
#
# Выходные данные
# Программа должна вывести слово YES, если точки находятся в одной координатной четверти, в противном случае
# вывести слово NO.

# TODO: взять ф-ию из класса
def sign(x):
    if x == 0:
        return 0
    elif x > 0:
        return 1
    else:
        return -1


def isCoor(x1: float, y1: float, x2: float, y2: float):
    """
    :return :str
    :type x1: float
    :type y1: float
    :type x2: float
    :type y2: float
    """

    YES: str = 'YES'
    NO: str = 'NO'

    if (sign(x1) == sign(x2)) & (sign(y1) == sign(y2)):
        return YES
    else:
        return NO


x1 = float(input())
y1 = float(input())

x2 = float(input())
y2 = float(input())

print(isCoor(x1, y1, x2, y2))
