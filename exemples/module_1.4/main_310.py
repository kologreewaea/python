# адача №310. Проверка на простоту_0
# Проверьте, является ли число простым.
#
# Входные данные
# Вводится одно натуральное число n не превышающее 2000000000 и не равное 1.
#
# Выходные данные
# Необходимо вывести  строку prime, если число простое, или composite, если число составное.
#
# Примеры

def is_prime(x):
    i = 1
    step = 1
    res = True
    while True:
        i += step
        if x % i == 0:
            res = False
            break

    return res


prime = 'prime'
composite = 'composite'

n = int(input())

if is_prime(n):
    print(prime)
else:
    print(composite)
