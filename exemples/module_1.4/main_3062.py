# Задача №3062. Утренняя пробежка
# В первый день спортсмен пробежал x километров, а затем он каждый день увеличивал пробег на 10% от предыдущего значения.
# По данному числу y определите номер дня, на который пробег спортсмена составит не менее y километров.
#
# Входные данные
# Программа получает на вход действительные числа x и y

def run(distance: float, max_distance: int) -> int:
    """

    :param distance: float
    :param max_distance: int
    :return: int
    """
    day = 1
    while distance < max_distance:
        distance += distance * 0.1
        day += 1
    return day


x = float(input())
y = int(input())

print(run(x, y))
