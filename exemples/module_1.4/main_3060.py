# Задача №3060. Точная степень двойки
# Дано натуральное число N. Выведите слово YES, если число N является точной степенью двойки, или слово NO в
# противном случае.
#
# Операцией возведения в степень пользоваться нельзя!
#
# Входные данные
# Вводится натуральное число.
#
# Выходные данные
# Выведите ответ на задачу.
#

import math

YES = 'YES'
NO = 'NO'
x = int(input())

while x % 2 == 0:
    x //= 2
if x == 1:
    print(YES)
else:
    print(NO)

