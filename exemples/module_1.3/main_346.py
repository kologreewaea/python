# Задача №346. Подсчет чисел
# Подсчитайте, сколько среди данных N чисел нулей, положительных чисел, отрицательных чисел.
#
# Входные данные
# Вводится число N, а затем N целых чисел.
#
# Выходные данные
# Необходимо вывести сначала число нулей, затем число положительных и отрицательных чисел.

N = int(input())

count_null = 0
count_positive = 0
count_negative = 0

for i in range(N):
    n = int(input())
    if n == 0:
        count_null += 1
        continue
    if n > 0:
        count_positive += 1
        continue
    if n < 0:
        count_negative += 1


print(count_null, count_positive, count_negative)
