# Задача №3074. Числа Фибоначчи
# Последовательность Фибоначчи определяется так:
#
# φ0=0,  φ1=1, ..., φn=φn-1+φn-2.
#
# По данному числу n определите n-е число Фибоначчи φn.
#
# Входные данные
# Вводится натуральное число n.
#
# Выходные данные
# Выведите ответ на задачу.
import time


def my_fibonacci(N: int) -> int:
    ar = list()
    k1 = 0
    k2 = 1
    for i in range(N):
        if i > 1:
            k1 = ar[i - 1]
        if i > 2:
            k2 = ar[i - 2]
        ar.append(k1 + k2)
    return ar[-1]


# решение быстрее моего (my_fibonacci) в 2 раза в значении от 100
def fibonacci(n: int) -> int:
    if n == 0:
        b = 0
    else:
        a, b = 0, 1
        for i in range(2, n + 1):
            a, b = b, a + b
    return b


N = int(input())
print(fibonacci(N))
