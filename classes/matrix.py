class Matrix:

    @staticmethod
    def input_int(n):
        return [[int(x) for x in input().split()] for _ in range(n)]

    @staticmethod
    def print(tab):
        for row in tab:
            for x in row:
                print(x, end="\t")
            print()

    @classmethod
    def __count_symmetric_row__(cls, tab, n):
        count = 0
        for i in range(n):
            for j in range(n):
                if (i != j) & (tab[i][j] == tab[j][i]):
                    count += 1
        return count

    def is_symmetric(self, tab, n):
        return 'yes' if self.__count_symmetric_row__(tab, n) == (n * (n - 1)) else 'no'

    @staticmethod
    def sum(tab):
        result = 0
        for row in tab:
            for x in row:
                result += x
        return result
   