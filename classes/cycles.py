# Класс для решения задач на циклы
class Cycle:
    YES = 'YES'
    NO = 'NO'

    @staticmethod
    def sum_square(n: int) -> int:
        """
         Функция вычисляет Сумму квадратов (1**2+2**2+...+n**2)
        :param n: int
        :return: n
        """
        result = 0
        for i in range(n + 1):
            result += i ** 2
        return result

    @staticmethod
    def list_of_even_numbers(a: int, b: int) -> str:
        """
        Возвращает последовательность четных чисел в промежутке a..b
        :param a: int
        :param b: int
        :return: string
        """
        string = ''
        for i in range(a, b + 1):
            if i % 2 == 0:
                string += ' ' + str(i)
        return string

    def find_num(self, needle: int, haystack: int) -> str:
        """
        Возвращает YES, если в haystack найдено число needle
        :param x: int
        :return: string
        """
        for i in range(haystack):
            k = int(input())
            if k == needle:
                return self.YES
                break
        else:
            return self.NO

    @staticmethod
    def fucktorial(N: int) -> int:
        """
        Вычисляет N! – произведение всех натуральных чисел от 1 до N ( N!=1∙2∙3∙…∙ N ).
        :param N: int
        :return: int
        """
        f = 1
        for i in range(N + 1):
            f = f * i
        return f

    @staticmethod
    def fibonacci(n: int) -> int:
        """
        По данному числу n вычисляет n-е число Фибоначчи φn.
        :param n: int
        :return: int
        """
        if n == 0:
            b = 0
        else:
            a, b = 0, 1
            for i in range(2, n + 1):
                a, b = b, a + b
        return b

    @staticmethod
    def squares(N: int) -> list:
        """
        Возвращает все точные квадраты натуральных чисел, не превосходящие данного числа N.
        :param N: int
        :return: list
        """
        list = []
        base = 2
        i = 1

        while N >= i ** base:
            list.append(i ** base)
            i += 1
        return list

    @staticmethod
    def divisors_of_number(x: int) -> list:
        """
        Возвращает все натуральные делители числа x в порядке возрастания (включая 1 и само число).
        :param x: int
        :return: list
        """
        return [i for i in range(1, x + 1) if x % i == 0]

    @staticmethod
    def min_del(x: int) -> int:
        min = 2

        while x % min != 0 and min ** 2 <= x:
            min += 1
            if x % min == 0:
                break

        if x % min == 0:
            return min
        else:
            return x

    @staticmethod
    def degrees(N: int) -> list:
        """
        Список степеней двойки
        :param N: int
        :return: list
        """
        list = []
        base = 2
        i = 0

        while N >= base ** i:
            list.append(base ** i)
            i += 1
        return list

    def if_two(self, x: int) -> str:
        """
        определяет является ли x точной степенью двойки,
        :param x:
        :return:
        """
        while x % 2 == 0:
            x //= 2
        if x == 1:
            return self.YES
        else:
            return self.NO
# Класс для решения задач на циклы
class Cycle:
    YES = 'YES'
    NO = 'NO'

    @staticmethod
    def sum_square(n: int) -> int:
        """
         Функция вычисляет Сумму квадратов (1**2+2**2+...+n**2)
        :param n: int
        :return: n
        """
        result = 0
        for i in range(n + 1):
            result += i ** 2
        return result

    @staticmethod
    def list_of_even_numbers(a: int, b: int) -> str:
        """
        Возвращает последовательность четных чисел в промежутке a..b
        :param a: int
        :param b: int
        :return: string
        """
        string = ''
        for i in range(a, b + 1):
            if i % 2 == 0:
                string += ' ' + str(i)
        return string

    def find_num(self, needle: int, haystack: int) -> str:
        """
        Возвращает YES, если в haystack найдено число needle
        :param x: int
        :return: string
        """
        for i in range(haystack):
            k = int(input())
            if k == needle:
                return self.YES
                break
        else:
            return self.NO

    @staticmethod
    def fucktorial(N: int) -> int:
        """
        Вычисляет N! – произведение всех натуральных чисел от 1 до N ( N!=1∙2∙3∙…∙ N ).
        :param N: int
        :return: int
        """
        f = 1
        for i in range(N + 1):
            f = f * i
        return f

    @staticmethod
    def fibonacci(n: int) -> int:
        """
        По данному числу n вычисляет n-е число Фибоначчи φn.
        :param n: int
        :return: int
        """
        if n == 0:
            b = 0
        else:
            a, b = 0, 1
            for i in range(2, n + 1):
                a, b = b, a + b
        return b

    @staticmethod
    def fibonacci_generate(n: int) -> list:
        """
        По данному числу n генерирует посл-ть Фибоначчи φn. до n
        :param n: int
        :return: list
        """
        a, b = 0, 1
        for i in range(2, n + 1):
            a, b = b, a + b
            yield b

    @staticmethod
    def squares(N: int) -> list:
        """
        Возвращает все точные квадраты натуральных чисел, не превосходящие данного числа N.
        :param N: int
        :return: list
        """
        list = []
        base = 2
        i = 1

        while N >= i ** base:
            list.append(i ** base)
            i += 1
        return list

    @staticmethod
    def divisors_of_number(x: int) -> list:
        """
        Возвращает все натуральные делители числа x в порядке возрастания (включая 1 и само число).
        :param x: int
        :return: list
        """
        return [i for i in range(1, x + 1) if x % i == 0]

    @staticmethod
    def min_del(x: int) -> int:
        min = 2

        while x % min != 0 and min ** 2 <= x:
            min += 1
            if x % min == 0:
                break

        if x % min == 0:
            return min
        else:
            return x

    @staticmethod
    def degrees(N: int) -> list:
        """
        Список степеней двойки
        :param N: int
        :return: list
        """
        list = []
        base = 2
        i = 0

        while N >= base ** i:
            list.append(base ** i)
            i += 1
        return list

    def if_two(self, x: int) -> str:
        """
        определяет является ли x точной степенью двойки,
        :param x:
        :return:
        """
        while x % 2 == 0:
            x //= 2
        return self.YES if x == 1 else self.NO

    @staticmethod
    def run(distance: float, max_distance: int) -> int:
        """
        По данному числу y определите номер дня, на который пробег спортсмена составит не менее y километров,
        если В первый день спортсмен пробежал x километров, а затем он каждый день увеличивал пробег на 10%
        от предыдущего значения.
        :param distance: float
        :param max_distance: int
        :return: int
        """
        day = 1
        while distance < max_distance:
            distance += distance * 0.1
            day += 1
        return day
