# Класс для решения задач на Условный оператор. Сложные условия
import math
from main import Main


class Conditional:
    YES = 'YES'
    NO = 'NO'

    def isLeapYear(self, year: int):
        """
        Функция возвращает слово YES, если год является високосным и NO - в противном случае.
        :param year: int
        :return: str
        """
        if (year % 4 == 0) & (year % 100 != 0):
            return self.YES
        elif year % 400 == 0:
            return self.YES
        else:
            return self.NO

    def chessLadij(self, x1: int, y1: int, x2: int, y2: int) -> str:
        """
        Функция возвращает слово YES, если ладья сможет побить фигуру за 1 ход и NO - в противном случае
        :param x1: int 1 - 8
        :param y1: int 1 - 8
        :param x2: int 1 - 8
        :param y2: int 1 - 8
        :return: str
        """

        return self.YES if (x1 == x2) | (y1 == y2) else self.NO


    def chessFers(self, x1: int, y1: int, x2: int, y2: int) -> str:
        """
        Функция возвращает слово YES, если ферзь сможет побить фигуру за 1 ход и NO - в противном случае
        :param x1: int 1 - 8
        :param y1: int 1 - 8
        :param x2: int 1 - 8
        :param y2: int 1 - 8
        :return: str
        """
        return self.YES if (abs(x1 - x2) == (abs(y1 - y2))) | (x1 == x2) | (y1 == y2) else self.NO


    def chessHorse(self, x1: int, y1: int, x2: int, y2: int) -> str:
        """
        Функция определяет, бьет ли конь, стоящий на клетке с указанными координатами (номер строки и номер столбца),
        фигуру, стоящую на другой указанной клетке.
        :type x1: int
        :type y1: int
        :type x2: int
        :type y2: int
        :return :str
        """

        return self.YES if {abs(x1 - x2), abs(y1 - y2)} == {1, 2} else  self.NO


    def canBeBrokenOff(self, n: int, m: int, k: int) -> str:
        """
        Функция определяет, можно ли от шоколадки размером n × m долек отломить k долек, если разрешается сделать
        один разлом по прямой между дольками (то есть разломить шоколадку на два прямоугольника).
        :param n: int
        :param m: int
        :param k: int
        :return: str
        """
        if ((k % n == 0) | (k % m == 0)) & (k < n * m):
            return self.YES
        else:
            return self.NO

    def isCoor(self, x1: float, y1: float, x2: float, y2: float) -> str:
        """

        :type x1: float
        :type y1: float
        :type x2: float
        :type y2: float
        :return :str
        """
        return self.YES if (Main.sign(x1) == Main.sign(x2)) & (Main.sign(y1) == Main.sign(y2)) else self.NO

    @staticmethod
    def quadratic(a: float, b: float, c: float):
        """
        Функция возвращает множество всех решений квадратного уравнения ax2 + bx + c = 0. С точностью до 5 знаков
        При отсутствии действительных корней return false
        :param a: float
        :param b: float
        :param c: float
        :return: mixed
        """
        D = b ** 2 - 4 * a * c

        if D > 0:
            x1 = (-b + math.sqrt(D)) / (2 * a)
            x2 = (-b - math.sqrt(D)) / (2 * a)

            return {'{:.5f}'.format(x1), '{:.5f}'.format(x2)}

        elif D == 0:
            x = -b / (2 * a)
            return {'{:.5f}'.format(x)}

        return False

    @staticmethod
    def isTriangle(a: int, b: int, c: int) -> bool:
        """
        Функция определяет составляют ли 3 введенные числа треугольник
        :param a: int
        :param b: int
        :param c: int
        :return: bool
        """
        return (a + b > c) & (b + c > a) & (a + c > b)

    @staticmethod
    def grade(a: int, b: int, c: int):
        """
        Функция возвращает значение угла в градусах по известным 3ем сторонам треугольника
        :param a: int
        :param b: int
        :param c: int
        :return: float
        """
        n = (a ** 2 + b ** 2 - c ** 2) / (2 * a * b)

        return math.degrees(math.acos(n))

    def TypeTriangle(self, a: float, b: float, c: float):
        """
        Функция возвращает тип трегульника по известным 3ем сторонам
        :param a: float
        :param b: float
        :param c: float
        :return: string
        """
        RIGHT = 'right'
        ACUTE = 'acute'
        OBTUS = 'obtuse'
        IMPOS = 'impossible'

        if not (self.isTriangle(a, b, c)):
            return IMPOS

        Triangle: float = max(self.grade(a, b, c), self.grade(c, a, b), self.grade(b, c, a))

        if Triangle == 90:
            return RIGHT
        elif Triangle > 90:
            return OBTUS
        elif Triangle < 90:
            return ACUTE

    @staticmethod
    def sclon(x: int) -> str:
        """
        Функция склоняет слово во множ числе по введенному кол-ву
        :param x: int
        :return: str
        """
        last_num = Main.last_mum(x)  # находит последнюю цифру натурального числа

        if (last_num in {1}) and (x != 11):
            return 'a'
        elif (last_num in {2, 3, 4}) and ((x > 19) | (x < 10)):
            return 'y'
        else:
            return ''
