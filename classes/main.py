# Класс для решения простых арифметических задач
import math


class Main:

    # Мат ф-ия sign(x)
    @staticmethod
    def sign(x):
        if x == 0:
            return 0
        elif x > 0:
            return 1
        else:
            return -1

    # Сумма чисел
    # Функция вычисляет сумму двух целых неотрицательных чисел
    # (длина каждого из чисел не превосходит 250)
    @staticmethod
    def add(x, y):
        return int(x) + int(y)

    # Функция находит предыдущее целое число
    @staticmethod
    def prev(x):
        return int(x) - 1

    # Функция находит следующее целое число
    @staticmethod
    def next(x):
        return int(x) + 1

    # Гипотенуза
    # Дано два числа a и b. Найдите гипотенузу треугольника с заданными катетами.
    @staticmethod
    def hypotenuse(a, b):
        import math
        return math.sqrt(a ** 2 + b ** 2)

    # Функция находит его последнюю цифру натурального числа.
    @staticmethod
    def last_mum(a):
        return a % 10

    # Функция считает расстояние между двух точек и возвращает одно число с тремя знаками в дробной части.
    # AB = √(x2 - x1)2 + (y2 - y1)2
    @staticmethod
    def distance(x1, y1, x2, y2):
        return f"{math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2):.3f}"

    # Круг
    # Функция вычисляет длину окружности круга по радиусу r.
    @staticmethod
    def length_circle(r):
        return round(math.pi * (r ** 2), 3)

    # Круг
    # Функция вычисляет  площадь круга по радиусу r.
    @staticmethod
    def square(r):
        return round(2 * math.pi * r, 3)

    # функция находит число десятков в  десятичной записи натурального числа
    @staticmethod
    def square(a):
        return a // 10 % 10

    # функция находит сумму цифр натурального числа.
    @staticmethod
    def sum(x):
        result = 0
        while ((x // 10) > 0):
            result += x % 10
            x = x // 10
        return result + x

    # Электронные часы
    # функция находит сколько прошло с начала суток по введенному кол-ву секунд (x)
    # и выводит время в формате h:mm:ss
    # Количество минут и секунд при необходимости дополняются до двузначного числа нулями.
    @staticmethod
    def main_date(x):
        hours = x // 60 ** 2
        if (hours > 23):
            hours = hours % 24
        x = x % 60 ** 2
        minutes = str(x // 60)
        seconds = str(x % 60)
        return str(hours) + ":" + minutes.zfill(2) + ":" + seconds.zfill(2)

    # Следующее четное
    # x - целое число
    # функция находит следующее за x четное число. При решении этой задачи нельзя использовать
    # условную инструкцию если и циклы.
    @staticmethod
    def next_even(x):
        return x - (x % 2) + 2
